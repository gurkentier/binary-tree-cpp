//
// Created by Dan on 26/11/2019.
//

#include "Node.h"

#include <utility>
#include <memory>

Node::Node(int x) {
    this->left = nullptr;
    this->right = nullptr;
    this->setKey(x);
}


void Node::setLeft(std::shared_ptr<Node> node) {
    this->left = std::move(node);
}
std::shared_ptr<Node> Node::getLeft() {
    return this->left;
}

void Node::setRight(std::shared_ptr<Node> node) {
    this->right = std::move(node);
}

std::shared_ptr<Node> Node::getRight() {
    return this->right;
}

void Node::setKey(int x) {
    this->key = x;
}

int Node::getKey() {
    return this->key;
}

void Node::addNode(std::shared_ptr<Node> node) {
    if(this->getKey() > node->getKey())
        if(!this->getLeft())
            this->setLeft(node);
        else
            this->getLeft()->addNode(node);
    else
        if(!this->getRight())
            this->setRight(node);
        else
            this->getRight()->addNode(node);
}







