//
// Created by Dan on 26/11/2019.
//

#ifndef BINARY_TREE_NODE_H
#define BINARY_TREE_NODE_H

#include <memory>
#include <iostream>

class Node {
public:
    Node(int x);

    void setLeft(std::shared_ptr<Node> node);
    std::shared_ptr<Node> getLeft();

    void setRight(std::shared_ptr<Node> node);
    std::shared_ptr<Node> getRight();


    void setKey(int x);
    int getKey();

    void addNode(std::shared_ptr<Node> node);

private:
    int key;
    std::shared_ptr<Node> left;
    std::shared_ptr<Node> right;

};


#endif //BINARY_TREE_NODE_H
