//
// Created by Dan on 26/11/2019.
//

#include "Node.h"
#include "Tree.h"

#include <memory>
#include <iostream>


Tree::Tree(std::shared_ptr<Node> node) {
    this->setRoot(node);
}

void Tree::setRoot(std::shared_ptr<Node> node) {
    this->root = std::move(node);
}

std::shared_ptr<Node> Tree::getRoot() {
    return this->root;
}

void Tree::addNode(std::shared_ptr<Node> node) {
    if(!this->getRoot())
        this->root = std::move(node);
    else
        this->root->addNode(node);
}

void Tree::inOrder(std::shared_ptr<Node> node) {
    if(node->getLeft())
        this->inOrder(node->getLeft());
    std::cout << node->getKey() << std::endl;
    if(node->getRight())
        this->inOrder(node->getRight());
}

std::shared_ptr<Node> Tree::search(int x) {
    std::shared_ptr<Node> tmp = this->getRoot();
    while(tmp)
        if(x < tmp->getKey())
            tmp = tmp->getLeft();
        else if(tmp->getKey() < x)
            tmp = tmp->getRight();
        else
            return tmp;
}

