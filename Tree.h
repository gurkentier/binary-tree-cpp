//
// Created by Dan on 26/11/2019.
//

#ifndef BINARY_TREE_TREE_H
#define BINARY_TREE_TREE_H

#include <memory>
#include <iostream>

#include "Node.h"



class Tree {
public:
    Tree(std::shared_ptr<Node> node);
    void setRoot(std::shared_ptr<Node> node);
    std::shared_ptr<Node> getRoot();
    void addNode(std::shared_ptr<Node> node);
    void inOrder(std::shared_ptr<Node> node);
    std::shared_ptr<Node> search(int x);
private:
    std::shared_ptr<Node> root;
};


#endif //BINARY_TREE_TREE_H
