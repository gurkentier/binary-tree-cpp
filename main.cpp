#include <iostream>
#include <memory>

#include "Node.h"
#include "Tree.h"

int main() {
    std::shared_ptr<Node> root = std::make_shared<Node>(1);
    std::shared_ptr<Tree> tree = std::make_shared<Tree>(root);

    tree->addNode(std::make_shared<Node>(5));
    tree->addNode(std::make_shared<Node>(123));
    tree->addNode(std::make_shared<Node>(1));
    tree->addNode(std::make_shared<Node>(1231));
    tree->addNode(std::make_shared<Node>(11));
    tree->addNode(std::make_shared<Node>(123));
    tree->addNode(std::make_shared<Node>(12323));

    tree->inOrder(tree->getRoot());
    std::shared_ptr<Node> searchResult = tree->search(5);
    std::cout << "Search result: " << searchResult->getKey() << std::endl;
    return 0;
}